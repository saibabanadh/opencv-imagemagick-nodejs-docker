FROM ubuntu:14.04
MAINTAINER saibabanadh<saibabanadh@gmail.com>
ENV DEBIAN_FRONTEND noninteractive

run apt-get update
run apt-get install -y software-properties-common python-software-properties
run add-apt-repository -y ppa:kubuntu-ppa/backports
run apt-get update
run apt-get install -y libcv-dev libcvaux-dev libhighgui-dev libopencv-dev
run curl -sL https://deb.nodesource.com/setup_5.x | bash -
run apt-get install -y nodejs imagemagick git unzip wget nano
